# Install Servers

## Configure

Please go to `vars.yml` and change some variables :
- `leader_ip` and `ips_list` to the server ip
- `domain` to your root domain

## Prerequisites

Assume that your os is Debian like
You have to install ansible-playbook

```
sudo apt-get install ansible sshpass
cd /root && git clone git@gitlab.com:nuitinfo1/deployer.git && git submodule init && git submodule update
```

And then install all role dependancies :

```
chmod +x deploy.sh
./deploy.sh -update
```

Install your server :
```
./deploy.sh -install
```

## How to deploy

```
./deploy.sh -nuitdelinfo
```

### Features

## Install
- Update the date of each node
- Call arnobirchler.ssh-access : setup user and ssh access 
- Call arnobirchler.tools : install some tools like (vim, git, tree, htop...)
- Call arnobirchler.docker : install docker
- Rename all hosts

