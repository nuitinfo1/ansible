#bin/bash

display_help()
{
    echo " -install,  Configure & install all hosts"
    echo " -init,     Init docker swarm"
    echo " -tools,    Deploy proxy and manager stack"
    echo " -db,       Deploy all databases"
    echo " -add-tools Deploy additionnal tools"
    echo " -java,     Deploy java" 
    echo " -csharp,   Deploy c#"
    echo " -nuitdelinfo, Deploy all for the nuit de l'info" 
    echo " -full"
}

if [ "$1" = "-update" ]
then 
    ansible-galaxy install -f -r requirements.yml
elif [ "$1" = "-install" ]
then
    ansible-playbook -i hosts install.yml
elif [ "$1" = "-init" ]
then
    ansible-playbook -i hosts swarm-init.yml
elif [ "$1" = "-tools" ]
then
    ansible-playbook -i hosts deploy_tools.yml
elif [ "$1" = "-add-tools" ]
then
    ansible-playbook -i hosts deploy_additionnal_tools.yml
elif [ "$1" = "-db" ]
then
    ansible-playbook -i hosts deploy_db.yml
elif [ "$1" = "-java" ]
then
    ansible-playbook -i hosts deploy_java.yml
elif [ "$1" = "-csharp" ]
then
    ansible-playbook -i hosts deploy_csharp.yml
elif [ "$1" = "-full" ]
then
    ansible-playbook -i hosts install.yml
    ansible-playbook -i hosts swarm-init.yml
    ansible-playbook -i hosts deploy_tools.yml
    ansible-playbook -i hosts deploy_db.yml
    ansible-playbook -i hosts deploy_java.yml
    ansible-playbook -i hosts deploy_csharp.yml
elif [ "$1" = "-nuitdelinfo" ]
then
    ansible-playbook -i hosts swarm-init.yml
    ansible-playbook -i hosts deploy_tools.yml
    ansible-playbook -i hosts deploy_db.yml
    ansible-playbook -i hosts deploy_java.yml
    ansible-playbook -i hosts deploy_angular.yml
    ansible-playbook -i hosts deploy_python.yml
    
    
elif [ "$1" = "-help" ]; then
    display_help
else 
    echo "unkown args, try -help"
fi;
